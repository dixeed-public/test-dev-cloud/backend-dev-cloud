module.exports = [
    {
        method: 'GET',
        path: '/test',
        options: {
            handler: () => 'content from the server'
        }
    }
]