'use strict';

const Confidence = require('confidence');
const Toys = require('toys');
const Schwifty = require('schwifty');

// Glue manifest as a confidence store
module.exports = new Confidence.Store({
  server: {
    host: '0.0.0.0',
    port: {
      $env: 'PORT',
      $coerce: 'number',
      $default: 3000,
    },
    debug: {
      $filter: { $env: 'NODE_ENV' },
      $default: {
        log: ['error'],
        request: ['error'],
      },
      production: {
        request: ['implementation'],
      },
    },
  },
  register: {
    plugins: [
      {
        plugin: '../lib', // Main plugin
        options: {},
      },
      {
        plugin: 'schwifty',
        options: {
          $filter: 'NODE_ENV',
          $default: {},
          $base: {
            migrateOnStart: true,
            knex: {
              client: 'pg',
              useNullAsDefault: true, // Suggested for sqlite3
              connection: {
                host: {
                  $env: 'DB_HOST',
                },
                port: {
                  $env: 'DB_PORT',
                },
                user: {
                  $env: 'DB_USER',
                },
                password: {
                  $env: 'DB_PWD',
                },
                database: {
                  $env: 'DB_NAME',
                },
              },

              migrations: {
                stub: Schwifty.migrationsStubPath,
              },
            },
          },
          production: {
            migrateOnStart: false,
          },
        },
      },
      {
        plugin: {
          $filter: { $env: 'NODE_ENV' },
          $default: 'hpal-debug',
          production: Toys.noop,
        },
      },
    ],
  },
});
