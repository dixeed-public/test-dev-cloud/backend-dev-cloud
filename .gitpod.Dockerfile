FROM gitpod/workspace-postgres

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

ENV DB_HOST localhost
ENV DB_USER gitpod
ENV DB_PORT 5432
ENV DB_NAME test-gitpod
ENV DB_PWD ''